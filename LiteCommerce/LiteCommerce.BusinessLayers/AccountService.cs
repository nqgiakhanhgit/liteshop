﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    /// <summary>
    /// Cung cấp các chức năng liên quan đến tài khoản của người dùng
    /// </summary>
  public  class AccountService
    {
        private static IAcuountDAL AccountDB;


        public static void Init(DatabaseTypes dbtype,string connectingString,AccountTypes accountType)
        {
            switch (dbtype)
            {
                case DatabaseTypes.SQLServer:
                    if (accountType == AccountTypes.Employee)
                        AccountDB = new DataLayers.SQL.EmployeeAccountDAL(connectingString);
                    else
                        AccountDB = new DataLayers.SQL.CustomerAccountDAL(connectingString);
                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }
        /// <summary>
        /// kiểm tra thông tin đăng nhập của tài khoản
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static Account Authorize(string userName, string password)
        {
            return AccountDB.Authorize(userName, password);
        }
        public static bool ChangepassWord(string accountid, string oldpass, string newpass)
        {
            return AccountDB.ChangePassword(accountid, oldpass, newpass);
        }
    }
    /// <summary>
    /// định nghĩa loại người dùng
    /// </summary>
    public enum AccountTypes
    {/// <summary>
    /// Nhân viên (dùng ứng dụng Litecommerce.Admin)
    /// </summary>
        Employee,
        /// <summary>
        /// Dùng litecommerce.shop
        /// </summary>
        Customer
    }
}
