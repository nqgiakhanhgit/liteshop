﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    public static class  DataService
    {
        private static ICountryDAL CountryDB;
        private static ICityDAL CityDB;
        private static ISupplierDAL SupplierDB;
        private static ICategory CategoryDB;
        private static ICustomer CustomerDB;
        private static IShipper ShipperDB;
        private static IEmployee EmployeeDB;
        /// <summary>
        /// Khởi tạo lớp tác nghiệp
        /// </summary>
        /// <param name="dbType">Loai Database</param>
        /// <param name="connection">Connecting String</param>
        

            
        public static void Init (DatabaseTypes dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseTypes.SQLServer:
                    CountryDB = new DataLayers.SQL.CountryDAL(connectionString);
                    CityDB = new DataLayers.SQL.CityDAL(connectionString);
                    SupplierDB = new DataLayers.SQL.SupplierDAL(connectionString);
                    CategoryDB = new DataLayers.SQL.CategoryDAL(connectionString);
                    EmployeeDB = new DataLayers.SQL.EmployeeDAL(connectionString);
                    CustomerDB = new DataLayers.SQL.CustomerDAL(connectionString);
                    ShipperDB = new DataLayers.SQL.ShipperDAL(connectionString);
                    break;
                case DatabaseTypes.FakeDB:
                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }
        #region Suppliers
        public static List<Country> ListCountries()
        {
            return CountryDB.List();
        }
        /// <summary>
        /// Hien thi Thanh Pho
        /// </summary>
        /// <returns></returns>
        public static List<City> ListCities()
        {
            return CityDB.List();
        }
        /// <summary>
        /// Hien thi thanh pho theo dat nuoc
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static List<City>ListCities(Country Name)
        {
            return CityDB.List(Name);
        }
        public static List<Supplier>ListSupplier(int page,int pageSize,string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;
            
            rowCount = SupplierDB.Count(seachValue);
   
            return SupplierDB.List(page, pageSize, seachValue);
        }
        public static Supplier GetSupplier(int supplierId)
        {
            return SupplierDB.Get(supplierId);
        }
        /// <summary>
        /// Them Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddSupplier(Supplier data)
        {
            return SupplierDB.Add(data);
        }
        /// <summary>
        /// Cap Nhat Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateSupplier(Supplier data)
        {
            return SupplierDB.Update(data);
        }
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public static bool Delete (int supplierId)
        {
            return SupplierDB.Delete(supplierId);
        }
        #endregion
        #region Category

        public static List<Category> ListCategory(int page, int pageSize, string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;

            rowCount = CategoryDB.Count(seachValue);

            return CategoryDB.List(page, pageSize, seachValue);
        }
        public static Category GetCategory(int CategoryId)
        {
            return CategoryDB.Get(CategoryId);
        }
        /// <summary>
        /// Them Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddCategory(Category data)
        {
            return CategoryDB.Add(data);
        }
        /// <summary>
        /// Cap Nhat Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateCategory(Category data)
        {
            return CategoryDB.Update(data);
        }
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public static bool DeleteCategory(int CategoryId)
        {
            return CategoryDB.Delete(CategoryId);
        }

        #endregion
        #region Employee
        

        public static List<Employee> ListEmployee(int page, int pageSize, string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;

            rowCount = EmployeeDB.Count(seachValue);

            return EmployeeDB.List(page, pageSize, seachValue);
        }
        public static List<Employee> ListEmployee()
        {

            return EmployeeDB.List();
        }
        public static Employee GetEmployee(int EmployeeId)
        {
            return EmployeeDB.Get(EmployeeId);
        }
        /// <summary>
        /// Them Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddEmployee(Employee data)
        {
            return EmployeeDB.Add(data);
        }
        /// <summary>
        /// Cap Nhat Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateEmployee(Employee data)
        {
            return EmployeeDB.Update(data);
        }
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public static bool DeleteEmployee(int EmployeeId)
        {
            return EmployeeDB.Delete(EmployeeId);
        }

        #endregion
        #region Customer
        public static List<Customer> ListCustomer(int page, int pageSize, string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;

            rowCount = CustomerDB.Count(seachValue);

            return CustomerDB.List(page, pageSize, seachValue);
        }
        public static List<Customer> ListCustomer()
        {
            return CustomerDB.List();
        }
        public static Customer GetCustomer(int CustomerId)
        {
            return CustomerDB.Get(CustomerId);
        }
        /// <summary>
        /// Them Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddCustomer(Customer data)
        {
            return CustomerDB.Add(data);
        }
        /// <summary>
        /// Cap Nhat Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateCustomer(Customer data)
        {
            return CustomerDB.Update(data);
        }
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public static bool DeleteCustomer(int CustomerId)
        {
            return CustomerDB.Delete(CustomerId);
        }
        #endregion
        #region Shipper
        public static List<Shipper> ListShipper(int page, int pageSize, string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;

            rowCount = ShipperDB.Count(seachValue);

            return ShipperDB.List(page, pageSize, seachValue);
        }
        public static Shipper GetShipper(int ShipperId)
        {
            return ShipperDB.Get(ShipperId);
        }
        /// <summary>
        /// Them Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddShipper(Shipper data)
        {
            return ShipperDB.Add(data);
        }
        /// <summary>
        /// Cap Nhat Nha Cung Cap
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateShipper(Shipper data)
        {
            return ShipperDB.Update(data);
        }
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public static bool DeleteShipper(int ShipperId)
        {
            return ShipperDB.Delete(ShipperId);
        }
        #endregion
    }
}
