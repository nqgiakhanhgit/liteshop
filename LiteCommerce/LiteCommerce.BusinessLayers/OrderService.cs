﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    public static class OrderService
    {

        private static IOrderDAL OrderDB;
        private static IOrderStatusDAL OrderStatusDB;

        /// <summary>
        /// Khởi tạo tính năng tác nghiệp (hàm này phải được gọi nếu muốn sử dụng
        /// các tính năng của lớp)
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        public static void Init(DatabaseTypes dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseTypes.SQLServer:
                    OrderDB = new DataLayers.SQL.OrderDAL(connectionString);
                    OrderStatusDB = new DataLayers.SQL.OrderStatusDAL(connectionString);
                    break;
                default:
                    throw new Exception("Database type is not supported");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="customerID"></param>
        /// <param name="employeeID"></param>
        /// <param name="shipperID"></param>
        /// <param name="searchValue"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>


        public static List<Order> List(int page, int pageSize, int customerId, int status, DateTime? fromDate, DateTime? today, out int rowCount)
        {
            rowCount = OrderDB.Count(fromDate, today, customerId, status);
            return OrderDB.List(page, pageSize, customerId, status, fromDate, today);
        }


        public static List<OrderStatus> ListOrderStatus()
        {
            return OrderStatusDB.List();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        /// 
        public static List<Order> ListEmployees()
        {
            return OrderDB.ListEmployees();
        }
        public static List<Order> Customers()
        {
            return OrderDB.ListCustomers();
        }
        public static List<Order> ListShippers()
        {
            return OrderDB.ListShippers();
        }



        public static Order Get(int orderID)
        {
            return OrderDB.Get(orderID);
        }


        public static int Add(Order data)
        {
            return OrderDB.Add(data);
        }
    }
}
