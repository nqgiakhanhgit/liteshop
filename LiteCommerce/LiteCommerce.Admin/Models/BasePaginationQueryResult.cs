﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerce.Admin.Models
{
    public abstract class BasePaginationQueryResult
    {
        /// <summary>
        ///  Lớp cơ sở cho tất cả các kết qyar truy vấn dữ liệu dưới dạng phân trang.
        /// </summary>
        /// 
        public int Page { get; set; }
        /// <summary>
        /// Trang Hiện tại
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Số trang
        /// </summary>
        /// 
        public string SeachValue { get; set; }
        /// <summary>
        /// Giá trị seach
        /// </summary>
        public int RowCount { get; set; }
        
        public int PageCount
        {
            get
            {
                int v = RowCount / PageSize;
                if (RowCount / PageSize > 0)
                {
                    v += 1;
                }
                return v;
            }
        }
        /// đếm số trang 
    }
}