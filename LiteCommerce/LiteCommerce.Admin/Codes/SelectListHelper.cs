﻿using LiteCommerce.BusinessLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin
{
    public static class SelectListHelper
    {
        /// <summary>
        /// Danh sách các quốc gia 
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> Countries()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach( var item in DataService.ListCountries())
            {
                list.Add(new SelectListItem()
                {
                    Value = item.CountryName,
                    Text = item.CountryName
                });
            }
            return list;
        }
        public static List<SelectListItem> Cities()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in DataService.ListCities())
            {
                list.Add(new SelectListItem()
                {
                    Value = item.CityName,
                    Text = item.CityName
                });
            }
            return list;
        }
        public static List<SelectListItem> Categories(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- Tất cả loại hàng --",
                });
            }
            foreach (var item in ProductService.ListCategories())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.CategoryID).ToString(),
                    Text = item.CategoryName,
                });
            }
            return list;
        }

        public static List<SelectListItem> Suppliers(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- Tất cả loại hàng --",
                });
            }
            foreach (var item in ProductService.ListSuppliers())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.SupplierID).ToString(),
                    Text = item.SupplierName,
                });
            }
            return list;
        }
        public static List<SelectListItem> Customers(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- Tất cả khách hàng --",
                });
            }
            foreach (var item in DataService.ListCustomer())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.CustomerID).ToString(),
                    Text = item.CustomerName,
                });
            }
            return list;
        }

        public static List<SelectListItem> Employee(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    //Value = "0",
                    Text = "-- Tất cả nhân viên --",
                });
            }
            foreach (var item in DataService.ListEmployee())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.EmployeeID).ToString(),
                    Text = item.FirstName
                });
            }
            return list;
        }

        /// <summary>
        /// Danh sách trạng thái
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> OrderStatus(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- Trạng thái --",
                });
            }
            foreach (var item in OrderService.ListOrderStatus())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.Status).ToString(),
                    Text = item.Description
                });
            }

            return list;
        }
        public static List<SelectListItem> Shippers(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- Trạng thái --",
                });
            }
            foreach (var item in OrderService.ListShippers())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.ShipperID).ToString(),
                    Text = item.ShipperName
                });
            }

            return list;
        }

    }


}