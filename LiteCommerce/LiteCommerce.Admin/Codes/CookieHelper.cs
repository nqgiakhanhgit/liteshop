﻿using LiteCommerce.DomainModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerce.Admin
{
    public class CookieHelper
    {
        /// <summary>
        /// chuyển đối tượng Account về json
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AccountToCookieString(Account value)
        {
            return JsonConvert.SerializeObject(value);
        }
        /// <summary>
        /// Chuyển ngược lại
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Account CookieStringToAccount(string value)
        {
            return JsonConvert.DeserializeObject<Account>(value);
        }
    }
}