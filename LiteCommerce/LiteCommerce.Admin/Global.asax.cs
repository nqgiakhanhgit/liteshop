﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LiteCommerce.BusinessLayers;

namespace LiteCommerce.Admin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Khai bao BSL
            string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
            DataService.Init(DatabaseTypes.SQLServer, connectionString);
            AccountService.Init(DatabaseTypes.SQLServer, connectionString, AccountTypes.Employee);
            ProductService.Init(DatabaseTypes.SQLServer, connectionString);
            OrderService.Init(DatabaseTypes.SQLServer, connectionString);
        }
    }
}
