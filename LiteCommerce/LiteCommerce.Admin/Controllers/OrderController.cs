﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Bổ sung đơn hàng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ sung đơn hàng";
            Order model = new Order()
            {
                OrderID = 0
            };

            return View(model);
        }

        public ActionResult List(string fromDate, string today, int page = 1, int statusId = 0, int customerId = 0)
        {

            DateTime OF = new DateTime();
            DateTime OT = new DateTime();
            if (!string.IsNullOrEmpty(fromDate))
            {
                OF = DateTime.Parse(fromDate);
            }
            else
            {
                OF = DateTime.Parse("1753, 01, 01");
            }
            if (!string.IsNullOrEmpty(today))
            {
                OT = DateTime.Parse(today);
            }
            else
            {
                OT = DateTime.Parse("1753-01-01");
            }
            int rowCount = 0;
            int pageSize = 10;
            var model = OrderService.List(page, pageSize, statusId, customerId, OF, OT, out rowCount);

            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.RowCount = rowCount;

            int pageCount = rowCount / pageSize;
            if (rowCount % pageSize > 0)
                pageCount += 1;
            ViewBag.PageCount = pageCount;

            //return Json(model, JsonRequestBehavior.AllowGet);
            return View(model);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Cập nhật thông tin của đơn hàng";
            var model = OrderService.Get(id);
            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        public ActionResult SaveOrder(Order data)
        {
            if (data.OrderID == 0)
            {

                var maxProductID = OrderService.Add(data);
                return RedirectToAction("Index", new { id = maxProductID });
            }
            return RedirectToAction("Index");
        }


    }

}