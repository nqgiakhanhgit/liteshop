﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        // GET: Categories
        public ActionResult Index()
        {
            return View();
           
        }
        public ActionResult List(int page = 1, string searchValue = "")
        {
            int pageSize = 25;
            int rowCount = 0;
            List<Category> categories = DataService.ListCategory(page, pageSize, searchValue, out rowCount);
            Models.CategoryPaginationResult model = new Models.CategoryPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = searchValue,
                Data = categories
            };
            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Nha Cung Cấp";
            Category model = new Category()
            {
                CategoryID = 0
            };
            return View("Edit", model);
        }
        /// <summary>
        /// Chỉnh Sửa Nhà Cung Cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Cập Nhật Thông Tin Category";
            Category model = DataService.GetCategory(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var model = DataService.GetCategory(id);
            if (model == null)
                return RedirectToAction("Index");
            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            else
            {
                DataService.DeleteCategory(id);
                return RedirectToAction("Index");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Save(Category data)
        {
            if (string.IsNullOrWhiteSpace(data.CategoryName))
            {

                ModelState.AddModelError("CategoryName", "Tên Không Được để Trống");
            }
            if (string.IsNullOrWhiteSpace(data.Description))
            {
                ModelState.AddModelError("Description", "Tên liên hệ không được đế trống");
            }

            //ModelState Lớp cho phép kiểm soát lỗi
            if (!ModelState.IsValid)
            {
                if (data.CategoryID == 0)
                {
                    ViewBag.Tile = " Bổ sung nhà cung cấp";

                }
                else
                    ViewBag.Title = " cập Nhật Nhà thông tin nhà cung cấp";
                return View("Edit", data);
            }
            if (data.CategoryID == 0)
            {

                DataService.AddCategory(data);
            }
            else
            {
                DataService.UpdateCategory(data);
            }
            //return Json(data, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index", "Categories");
        }

    }
}