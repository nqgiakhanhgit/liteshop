﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LiteCommerce.Admin.Controllers
{
    public class AccountController : Controller
    {
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    /// 
    /// cho phép truy cập khi chưa login
    [AllowAnonymous]
        public ActionResult Login( string username="", string password="")
        {
            if (Request.HttpMethod =="POST")
            {
                var account = AccountService.Authorize(username, CryptHelper.Md5(password));
                if(account == null)
                {
                    ModelState.AddModelError("", "Thông tin đăng nhập sai");
                    return View();
                }
                //return Json(account);
                FormsAuthentication.SetAuthCookie(CookieHelper.AccountToCookieString(account), false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
   

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
        [HttpGet]
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult ChangePassword(string accountId, string oldpass, string newpass, string reType = "")
        {
                if (AccountService.ChangepassWord(accountId, CryptHelper.Md5(oldpass), CryptHelper.Md5(newpass)) == true)
                {
                return RedirectToAction("Index", "Home");
            }
                else
                {
                ModelState.AddModelError("", "Mật Khẩu cũ không đúng");
                return View();
                }
            

         
        }

       

    }
}