﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        // GET: Customers
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(int page = 1, string searchValue = "")
        {
            int pageSize = 25;
            int rowCount = 0;
            List<Customer> customers = DataService.ListCustomer(page, pageSize, searchValue, out rowCount);
            Models.CustomerPaginationResult model = new Models.CustomerPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = searchValue,
                Data = customers
            };
            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Khách Hàng";
            Customer model = new Customer()
            {
                CustomerID = 0
            };
            return View("Edit", model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Cập Nhật Thông Tin Khách Hàng";
           Customer model = DataService.GetCustomer(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var model = DataService.GetCustomer(id);
            if (model == null)
                return RedirectToAction("Index");
            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            else
            {
                DataService.DeleteCustomer(id);
                return RedirectToAction("Index");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Save(Customer data)
        {
            if (string.IsNullOrWhiteSpace(data.CustomerName))
            {

                ModelState.AddModelError("CustomerName", "Tên Không Được để Trống");
            }
            if (string.IsNullOrWhiteSpace(data.ContactName))
                if (string.IsNullOrWhiteSpace(data.ContactName))
                {
                    ModelState.AddModelError("ContactName", "Tên liên hệ không được đế trống");
                }
            if (string.IsNullOrWhiteSpace(data.PostalCode))
            {
                data.PostalCode = "mã bưu chính không được để trống ";
            }
            if (string.IsNullOrWhiteSpace(data.Email))
            {
                data.Email = " Email Không được để rỗng";
            }
            if (string.IsNullOrWhiteSpace(data.Password))
            {
                data.Password = " Pass Không được để rỗng";
            }
            //ModelState Lớp cho phép kiểm soát lỗi
            if (!ModelState.IsValid)
            {
                if (data.CustomerID == 0)
                {
                    ViewBag.Tile = " Bổ sung Khách Hàng";

                }
                else
                    ViewBag.Title = " cập Nhật Khách Hàng";
                return View("Edit", data);
            }
            if (data.CustomerID == 0)
            {

                DataService.AddCustomer(data);
            }
            else
            {
                DataService.UpdateCustomer(data);
            }

            //return Json(data, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index", "Customers");
        }
    }
}

