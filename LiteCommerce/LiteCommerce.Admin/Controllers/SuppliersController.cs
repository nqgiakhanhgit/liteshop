﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LiteCommerce.BusinessLayers;


namespace LiteCommerce.Admin.Controllers
{
    /// <summary>
    /// atribute (action controller), Phạm vi tác động tất cả các action nếu khai báo ở đây
    /// Kiểm tra xem người sử dụng đã đăng nhập chưa, nếu chưa thì đá về ~/Account/Login
    /// </summary>
    [Authorize]
    public class SuppliersController : Controller
    {
        /// <summary>
        /// Tìm Kiếm, Hiển thị danh sách ncc
        /// </summary>
        /// <returns></returns>
        /// [allowanonymoous] truy cập mà không cần đăng nhập
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(int page = 1, string searchValue ="")
        {
            #region Comment
            // can chuyen cho view  : list cac supplier, rowcount,page,pageCount,
            //int rowCount = 0;
            //int pageSize = 25;

            //List<Supplier> suppliers = BusinessLayers.DataService.ListSupplier(page, pageSize,searchValue, out rowCount);
            //int pageCount = rowCount / pageSize;
            //if (rowCount % pageSize > 0)
            //{
            //    pageCount += 1;
            //}

            //ViewBag.Page = page;
            //ViewBag.RowCount = rowCount;
            //ViewBag.PageCount = pageCount;
            //return View(suppliers);
            #endregion
            // page, Seach Value
            //if (string.IsNullOrWhiteSpace(searchValue))
            //    searchValue = Convert.ToString(Session["Supplier_SeachValue"] = searchValue);

            //Session["Supplier_SeachValue"] = searchValue;

            //  searchValue = Convert.ToString(Session["Supplier_SeachValue"] = searchValue);

            int pageSize = 15;
            int rowCount = 0;
            List<Supplier> suppliers = DataService.ListSupplier(page, pageSize, searchValue, out rowCount);
            Models.SupplierPaginationResult model = new Models.SupplierPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = searchValue,
                Data = suppliers
            };

            return View(model);
        }
        /// <summary>
        /// Bổ Sung Nhà Cung Cấp
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Nha Cung Cấp";
            Supplier model = new Supplier()
            {
                SupplierID = 0
            };
            return View("Edit", model);
        }
        /// <summary>
        /// Chỉnh Sửa Nhà Cung Cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Cập Nhật Thông Tin Nhà Cung Cấp";
            Supplier model = DataService.GetSupplier(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var model = DataService.GetSupplier(id);
            if (model == null)
                return RedirectToAction("Index");
            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            else
            {
                DataService.Delete(id);
                return RedirectToAction("Index");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Save(Supplier data)
        {
            if (string.IsNullOrWhiteSpace(data.SupplierName))
            {

                ModelState.AddModelError("SupplierName", "Tên Không Được để Trống");
            }
            if (string.IsNullOrWhiteSpace(data.ContactName))
            {
                ModelState.AddModelError("ContactName", "Tên liên hệ không được đế trống");
            }
            if (string.IsNullOrWhiteSpace(data.PostalCode))
            {
                data.PostalCode = "mã bưu chính không được để trống ";
            }
            if (string.IsNullOrWhiteSpace(data.Phone))
            {
                data.Phone = " Số điện thoại không được để trống";
            }
            //ModelState Lớp cho phép kiểm soát lỗi
            if (!ModelState.IsValid)
            {
                if (data.SupplierID == 0)
                {
                    ViewBag.Tile = " Bổ sung nhà cung cấp";

                }
                else
                    ViewBag.Title = " cập Nhật Nhà thông tin nhà cung cấp";
                return View("Edit", data);
            }
            if (data.SupplierID == 0)
            {

                DataService.AddSupplier(data);
            }
            else
            {
                DataService.UpdateSupplier(data);
            }

            //return Json(data, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index", "Suppliers");
        }
    }

}