﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class ShippersController : Controller
    {
        // GET: Shippers
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int pageSize = 6;
            int rowCount = 0;
            List<Shipper> shippers = DataService.ListShipper(page, pageSize, searchValue, out rowCount);
            Models.ShipperPaginationResult model = new Models.ShipperPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = searchValue,
                Data = shippers
            };
            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Shipper";
            Shipper model = new Shipper()
            {
                ShipperID = 0
            };
            return View( model);
        }
        public ActionResult Delete (int id)
        {
            var model = DataService.GetShipper(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            DataService.DeleteShipper(id);
            return RedirectToAction("Index");

        }



        public ActionResult Save(Shipper data)
        {
            /*return Json(data, JsonRequestBehavior.AllowGet);*/
            if (string.IsNullOrWhiteSpace(data.ShipperName))
            {
                ModelState.AddModelError("ShipperName", "Tên  không được đế trống");
            }
            if (string.IsNullOrWhiteSpace(data.Phone))
            {
                ModelState.AddModelError("Phone", "Số điện thoại không được đế trống");
            }
            if (data.ShipperID == 0)
            {
                DataService.AddShipper(data);
            }
            else
            {
                DataService.UpdateShipper(data);
            }
            return RedirectToAction("Index", "Shippers");
        }
    }
}
