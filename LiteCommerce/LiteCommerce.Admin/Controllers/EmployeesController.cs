﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {
        // GET: Employees
        public ActionResult Index(int page = 1 , string seachValue = "")
        {
            int pageSize = 25;
            int rowCount = 0;
            List<Employee> employees = DataService.ListEmployee(page, pageSize, seachValue, out rowCount);
            Models.EmployeePaginationResult model = new Models.EmployeePaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = seachValue,
                Data = employees
            };
            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Employees";
            Employee model = new Employee()
            {
                EmployeeID = 0
            };
            return View(model);
        }
        public ActionResult Delete(int id)
        {
            var model = DataService.GetEmployee(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            DataService.DeleteEmployee(id);
            return RedirectToAction("Index");

        }



        public ActionResult Save(Employee data)
        {
            if (string.IsNullOrWhiteSpace(data.FirstName))
            {
                ModelState.AddModelError("FirstName", "Tên  không được đế trống");
            }
            if (string.IsNullOrWhiteSpace(data.LastName))
            {
                ModelState.AddModelError("LastName", "Họ  không được đế trống");
            }
            if (string.IsNullOrWhiteSpace(data.Email))
            {
                ModelState.AddModelError("Email", "Email  không được đế trống");
            }
            if (string.IsNullOrWhiteSpace(data.Password))
            {
                ModelState.AddModelError("Password", "Mật khẩu  không được đế trống");
            }
            if (data.EmployeeID == 0)
            {
                DataService.AddEmployee(data);
            }
            else
            {
                DataService.UpdateEmployee(data);
            }
            return RedirectToAction("Index", "Employees");
        }
    }
}
