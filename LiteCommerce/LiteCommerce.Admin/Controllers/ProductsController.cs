﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, int category = 0, int supplier = 0, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 10;
            var model = ProductService.List(page, pageSize, category, supplier, searchValue, out rowCount);

            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.RowCount = rowCount;

            int pageCount = rowCount / pageSize;
            if (rowCount % pageSize > 0)
                pageCount += 1;
            ViewBag.PageCount = pageCount;

            return View(model);
        }

        /// <summary>
        /// Bổ sung mặt hàng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ sung mặt hàng";
            Product model = new Product()
            {
                ProductID = 0
            };

            return View(model);
        }

        /// <summary>
        /// Bổ sung nhà cung cấp
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Chỉnh Sửa thông tin của mặt hàng";
            var model = ProductService.GetEx(id);
            if (model == null)
                return RedirectToAction("Index");

            return View("Edit",model);
        }


        //[HttpPost]
        //public ActionResult Edit(int ProductID, Product model, string ProductName, int SupplierID, int CategoryID, string Unit, decimal Price, string Photo)
        //{

        //    Product data = new Product()
        //    {
        //        ProductID = ProductID,
        //        ProductName = ProductName,
        //        SupplierID = SupplierID,
        //        CategoryID = CategoryID,
        //        Unit = Unit,
        //        Price = Price,
        //        Photo = Photo
        //    };
        //    ProductService.Update(data);
        //    return RedirectToAction("Index", "Products");
        //}


        public ActionResult DeleteAttributes(int id, long[] attributeIds)
        {
            if (attributeIds == null || attributeIds.Length == 0)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            ProductService.DeleteAttributes(attributeIds);
            return RedirectToAction("Edit", new { id = id });
        }
        public ActionResult DeleteGalleries(int id, long[] gallerieIds)
        {
            if (gallerieIds == null || gallerieIds.Length == 0)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            ProductService.DeleteGalleries(gallerieIds);
            return RedirectToAction("Edit", new { id = id });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Save(Product data)
        {
            //    return Json(data, JsonRequestBehavior.AllowGet);
            //}
            if (string.IsNullOrWhiteSpace(data.ProductName))
                ModelState.AddModelError("ProductName", "Tên hàng không được để trống (*)");
            if (string.IsNullOrWhiteSpace(data.Unit))
                ModelState.AddModelError("Unit", "Đơn vị tính không được để trống (*)");


            if (!ModelState.IsValid)
            {
                if (data.ProductID == 0)
                    return View("Add", data);
                else
                {
                    var editdata = ProductService.GetEx(data.ProductID);
                    return View("Edit",editdata);
                }
            }
                if (data.ProductID == 0)
                {

                    var maxProductID = ProductService.Add(data);
                    return RedirectToAction("Edit", new { id = maxProductID });
                }
                else
                {
                    ProductService.Update(data);
                }
            
            return RedirectToAction("Edit", new { id = data.ProductID });
        }
        public ActionResult SaveAttribute(ProductAttribute data)
        {
            //return Json(data, JsonRequestBehavior.AllowGet);
            if (data.AttributeID == 0)
            {
                ProductService.AddAttribute(data);
            }
            else
            {
                ProductService.UpdateAttribute(data);
            }
            return RedirectToAction("Edit", new { id = data.ProductId });
        }
        public ActionResult SaveGallery(ProductGallery data)
        {
            if (data.GalleryID == 0)
            {
                ProductService.AddGallery(data);
            }
            else
            {

                ProductService.UpdateGallery(data);
            }
            return RedirectToAction("Edit", new { id = data.ProductID });
        }
    }
}