﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// Đinh nghĩa phép xử lý dữ liệu liên quan đến mặt hàng
    /// </summary>
  public  interface IProductDAL
    {
        List<Product> ListCategories();

        List<Product> ListSuppliers();
        /// <summary>
        /// Danh sách mặt hàng tìm kiếm,lọc dữ liệu phân trang
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="categoryID">mã loại hàng cần lấy( 0 nếu lấy tất cả các loại hàng )</param>
        /// <param name="supplierID"> Mã Nhà cung cấp ( 0 Nếu lấy tất cả các nhà cung cấp )</param>
        /// <param name="seachValue">Tên mặt hàng ( tìm chuỗi rỗng nếu không tìm kiếm)</param>
        /// <returns></returns>
        List<Product> List(int page, int pageSize, int categoryID, int supplierID, string seachValue);

       
        /// <summary>
        /// Hỗ trợ cho phân trang dữ liệu ( đếm số lượng mặt hàng khi tìm kiếm , lọc dữ liệu )
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="supplierID"></param>
        /// <param name="seachValue"></param>
        /// <returns></returns>
        int Count(int categoryID, int supplierID, string seachValue);

        /// <summary>
        /// Lấy thông tin các mặt hàng theo mã 
        /// </summary>
        /// <param name="ProductID"></param>
        /// <returns></returns>
        Product Get(int ProductID);

        ProductEx GetEx(int productID);

        /// <summary>
        /// Thêm mặt hàng trả về list
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        int Add(Product data);



        bool Update(Product data);


        bool Delete(int productid);

        /// <summary>
        /// Danh sach cac thuoc tinh 1 product nao do theo id cua mat hang
        /// </summary>
        List<ProductAttribute> ListAttributes(int productId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        ProductAttribute GetAttribute(long attributeID);


        /// <summary>
        /// Theem
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        long AddAttribute(ProductAttribute data);
        /// <summary>
        /// Cap Nhat
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        /// 
        bool UpdateAttribute(ProductAttribute data);
        /// <summary>
        /// Xoa
        /// </summary>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        bool DeleteAttribute(long attributeID);



        /// <summary>
        /// Danh sach ah
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        List<ProductGallery> ListGalleries(int productID);

        /// <summary>
        /// Laays 1 cai anh
        /// </summary>
        /// <param name="galleryID"></param>
        /// <returns></returns>
        ProductGallery GetGallery(long galleryID);


        long AddGallery(ProductGallery data);

        bool UpdateGallery(ProductGallery data);


        bool DeleteGallery(long galleryID);
    }
}
