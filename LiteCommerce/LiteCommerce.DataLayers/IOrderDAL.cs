﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IOrderDAL
    {
        List<Order> ListShippers();
        List<Order> ListCustomers();
        List<Order> ListProducts();
        List<Order> ListEmployees();

        List<Order> List(int page, int pageSize, int statusId, int categoryId, DateTime? day, DateTime? today);

        int Count(DateTime? day, DateTime? today, int customerId, int statusId);


        Order Get(int OrderId);



        OrderEX GetEx(int OrderId);



        OrderDetailEX GetOderDetailEx(int OrderId);

        int Add(Order data);

        int AddOrderDetail(OrderDetail data);



        bool UpdateOrderDetail(OrderDetail data);



        bool Delete(int OrderId);

        bool DeleteOrderDetail(int orderdetailId);

        List<OrderDetail> ListOrderDetail(int orderdetailId);
    }
}
