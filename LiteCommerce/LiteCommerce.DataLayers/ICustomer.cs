﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface ICustomer
    {
        List<Customer> List();
        /// <summary>
        /// Hiển thị danh sách, Trang nào, Số Lượng hiển thị trên trang, và giá trị tìm kiếm
        /// </summary>
        /// /// <param name="pages">Trang cần lấy dữ luêu</param>
        /// <param name="pagesSize">Số dòng trên mỗi trang</param>
        /// <param name="seachValue">Giá trị cần tìm kiếm (để rỗng lấy toàn bộ)</param>
        /// <returns></returns>

        //List<Supplier> List(int page, int pageSize, string seachValue);
        List<Customer> List(int page, int pageSize, string seachValue);        /// <summary>
                                                                               /// Đếm số lượng nhà cung cấp
                                                                               /// </summary>
                                                                               /// <param name="seachValue">Giá trị cần tìm kiếm (để rỗng lấy toàn bộ)</param>
                                                                               /// <returns></returns>
                                                                               /// 

        int Count(string seachValue);

        /// <summary>
        /// Lấy Thông tin của một nhà cung cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 

        Customer Get(int customerId);

        /// <summary>
        /// Bổ sung 1 nhà cung cấp trả về mã của nhà cung cấp cần được bổ sung
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>

        int Add(Customer data);

        /// <summary>
        /// Cập nhật thông tin nhà cung cấp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Update(Customer data);

        /// <summary>
        /// Xóa Một nhà cung cấp
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <returns></returns>
        bool Delete(int CustomerID);


    }
}