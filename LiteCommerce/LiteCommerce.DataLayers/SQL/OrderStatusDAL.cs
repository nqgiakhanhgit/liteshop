﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;

namespace LiteCommerce.DataLayers.SQL
{
    public class OrderStatusDAL : _BaseDAL, IOrderStatusDAL
    {
        public OrderStatusDAL(string connectionString) : base(connectionString)
        {

        }

        public List<OrderStatus> List()
        {
            List<OrderStatus> data = new List<OrderStatus>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM dbo.OrderStatus";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new OrderStatus()
                        {
                            Status = Convert.ToInt32(dbReader["Status"]),
                            Description = Convert.ToString(dbReader["Description"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }


    }
}
