﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using LiteCommerce.DomainModels;

namespace LiteCommerce.DataLayers.SQL
{
    public class CountryDAL : _BaseDAL, ICountryDAL
    {
        /// <summary>
        /// bản thân nó không cần xử lý mà lên lớp cha xử lý
        /// </summary>
        /// <param name="connectionString"></param>
        public CountryDAL(string connectionString) : base(connectionString)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Country> List()
        {
            List<Country> data = new List<Country>();
            // Xử lý đã được viết ở base ( mở kết nối ) 
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from Countries";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                /// Thực thi câu lệnh trả về lưu trong dbreader ( tập các dòng các cột của các bảng )
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Country()
                        {
                            CountryName = Convert.ToString(dbReader["CountryName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
    }
}
