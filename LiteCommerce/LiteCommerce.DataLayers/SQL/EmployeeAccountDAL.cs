﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data;
using System.Data.SqlClient;
namespace LiteCommerce.DataLayers.SQL
{
    /// <summary>
    /// cài đặc các tính năng liên quan đến tài khoản của nhân viên (employee)
    /// </summary>
    public class EmployeeAccountDAL : _BaseDAL, IAcuountDAL
    {
        public EmployeeAccountDAL(String connectingstring) : base(connectingstring)
        {

        }
        public Account Authorize(string userName, string password)
        {
            Account data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select EmployeeID , FirstName, LastName, photo, Email from Employees where Email = @email AND Password = @password ";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@email", userName);
                cmd.Parameters.AddWithValue("@password", password);


                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Account()
                        {
                            AccountID = dbReader["EmployeeID"].ToString(),
                            FullName = $"{dbReader["FirstName"]}{dbReader["LastName"]}",
                            Email = dbReader["Email"].ToString(),
                            photo = dbReader["photo"].ToString()

                        };
                    }
                    connection.Close();
                }
                return data;
            }




        }

        public bool ChangePassword(string accountId, string oldPassword, string newPassword)
        {
            int rowsAffected = 0;
            using (SqlConnection connection = GetConnection())
            {


                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"UPDATE Employees SET Password=@newpass WHERE EmployeeID = @employeeid AND Password= @oldpass  ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@employeeid", accountId);
                cmd.Parameters.AddWithValue("@newpass", newPassword);
                cmd.Parameters.AddWithValue("@oldpass", oldPassword);

                rowsAffected = Convert.ToInt32(cmd.ExecuteNonQuery());

                connection.Close();
            }

            return rowsAffected > 0;
        }
    }
}
