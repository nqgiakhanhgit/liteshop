﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data;
using System.Data.SqlClient;

namespace LiteCommerce.DataLayers.SQL
{
    public class CustomerDAL : _BaseDAL, ICustomer
    {
        public CustomerDAL(string connectingString) : base(connectingString)
        {

        }
        public int Add(Customer data)
        {
            int customerId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO dbo.Customers

	        (
                CustomerName ,
	          ContactName ,
	          Address ,
	          City ,
	          PostalCode ,
	          Country ,
	          Email ,
	          Password
	        )
	VALUES  (
            @CustomerName,
	         @ContactName,
	         @Address,
	          @City ,
	         @PostalCode ,
	          @Country ,
	    @Email,
@Password
	        )
select @@IDENTITY";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
             //   cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@CustomerName", data.CustomerName);
                cmd.Parameters.AddWithValue("@ContactName", data.ContactName);
                cmd.Parameters.AddWithValue("@Address", data.Address);
                cmd.Parameters.AddWithValue("@City", data.City);
                cmd.Parameters.AddWithValue("@PostalCode", data.PostalCode);
                cmd.Parameters.AddWithValue("@Country", data.Country);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", data.Password);
                customerId = Convert.ToInt32(cmd.ExecuteScalar());// 1 dong 1 cot 1 gia tri
                connection.Close();
            }

            return customerId;
        }

        public int Count(string seachValue)
        {
            if (seachValue != "")
            {
                seachValue = "%" + seachValue + "%";
            }
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select count (*)  from Customers where @seachValue like N'' or CustomerName like @seachValue or ContactName like @seachValue";
                cmd.Parameters.AddWithValue("@seachValue", seachValue);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                count = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return count;
        }

        public bool Delete(int CustomerID)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE  FROM Customers where CustomerId = @customerId
                    AND NOT EXISTS(SELECT * FROM Orders
				WHERE CustomerID = Customers.CustomerID)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@customerId", CustomerID);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }

        public Customer Get(int customerId)
        {
            Customer data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from Customers Where customerID = @customerId";
                cmd.Parameters.AddWithValue("@customerId", customerId);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Customer()
                        {
                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            CustomerName = Convert.ToString(dbReader["CustomerName"]),
                            ContactName = Convert.ToString(dbReader["ContactName"]),
                            Address = Convert.ToString(dbReader["Address"]),
                            City = Convert.ToString(dbReader["City"]),
                            PostalCode = Convert.ToString(dbReader["PostalCode"]),
                            Country = Convert.ToString(dbReader["Country"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Password = Convert.ToString(dbReader["Password"])
                        };
                    }

                    connection.Close();

                }
            }
            return data;
        }

        public List<Customer> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Customer> data = new List<Customer>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from
                                    (
                                        select *, ROW_NUMBER() OVER(ORDER by ContactName) as RowNumber

                                        from Customers

                                        where (@searchValue = '')

                                            OR(
                                                CustomerName LIKE @searchValue

                                                or ContactName Like @searchValue
                                                
                                            )
                                    ) AS s
                                    where s.RowNumber between(@page -1)*@pageSize + 1 and @page*@pageSize
                                    Order by s.RowNumber";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Customer()
                        {
                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            CustomerName = Convert.ToString(dbReader["CustomerName"]),
                            ContactName = Convert.ToString(dbReader["ContactName"]),
                            Address = Convert.ToString(dbReader["Address"]),
                            City = Convert.ToString(dbReader["City"]),
                            PostalCode = Convert.ToString(dbReader["PostalCode"]),
                            Country = Convert.ToString(dbReader["Country"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Password = Convert.ToString(dbReader["Password"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<Customer> List()
        {
            List<Customer> data = new List<Customer>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from Customers";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Customer()
                        {
                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            CustomerName = Convert.ToString(dbReader["CustomerName"]),
                            ContactName = Convert.ToString(dbReader["ContactName"]),
                            Address = Convert.ToString(dbReader["Address"]),
                            City = Convert.ToString(dbReader["City"]),
                            PostalCode = Convert.ToString(dbReader["PostalCode"]),
                            Country = Convert.ToString(dbReader["Country"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Password = Convert.ToString(dbReader["Password"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public bool Update(Customer data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.Customers 
SET CustomerName =@CustomerName,
	ContactName=@CotactName,
	Address = @Address,
	City = @City,
	PostalCode = @PostalCode,
	Country =@Country,
	Email =@Email
    Password = @Password
	WHERE CustomerID = @CustomerID";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@CustomerID", data.CustomerID);
                cmd.Parameters.AddWithValue("@CustomerName", data.CustomerName);
                cmd.Parameters.AddWithValue("@CotactName", data.ContactName);
                cmd.Parameters.AddWithValue("@Address", data.Address);
                cmd.Parameters.AddWithValue("@PostalCode", data.PostalCode);
                cmd.Parameters.AddWithValue("@City", data.City);
                cmd.Parameters.AddWithValue("@Country", data.Country);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", data.Password);



                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
    }
    
}
