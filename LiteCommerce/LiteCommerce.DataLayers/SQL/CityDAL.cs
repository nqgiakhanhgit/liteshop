﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using LiteCommerce.DomainModels;

namespace LiteCommerce.DataLayers.SQL
{
    public class CityDAL : _BaseDAL, ICityDAL
    {
        /// <summary>
        /// bản thân nó không cần xử lý mà lên lớp cha xử lý
        /// </summary>
        /// <param name="connectionString"></param>
        public CityDAL(string connectionString) : base(connectionString)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<City> List()
        {
            List<City> data = new List<City>();
            // Xử lý đã được viết ở base ( mở kết nối ) 
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from cities";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                /// Thực thi câu lệnh trả về lưu trong dbreader ( tập các dòng các cột của các bảng )
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                        data.Add(new City()
                        {
                            CityName = Convert.ToString(dbReader["CityName"]),
                            CountryName = Convert.ToString(dbReader["CountryName"])

                        });
                }

                connection.Close();
            }
            return data;
        }


        public List<City> List(Country name)
        {
            List<City> data = new List<City>();
            // Xử lý đã được viết ở base ( mở kết nối ) 
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from cities where countryname = @countryname";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@countryname", name);
                cmd.Connection = connection;
                /// Thực thi câu lệnh trả về lưu trong dbreader ( tập các dòng các cột của các bảng )
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                        data.Add(new City()
                        {
                            CityName = Convert.ToString(dbReader["CityName"]),
                            CountryName = Convert.ToString(dbReader["CountryName"])

                        });
                }

                connection.Close();
            }
            return data;
        }


    }
}

