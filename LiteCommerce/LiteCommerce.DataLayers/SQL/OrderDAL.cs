﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers.SQL
{
    public class OrderDAL : _BaseDAL, IOrderDAL
    {
        public OrderDAL(string connectingString) : base(connectingString)
        {

        }
        public int Add(Order data)
        {
            int orderId = 0;
            using (SqlConnection connection = GetConnection())
            {

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO Orders
                                    (
	                                             CustomerID,
                                                    OrderTime,
                                                    EmployeeID,
                                                    ShipperID,
                                                    Status
                                    )
                                    VALUES
                                    (
	                                    @CustomerID,
	                                     GETDATE(),
                                        @EmployeeID,
                                        @ShipperID,
                                         @Status
                                    )
                                    SELECT @@IDENTITY";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@CustomerID", data.CustomerID);
                cmd.Parameters.AddWithValue("@EmployeeID", data.EmployeeID);
                cmd.Parameters.AddWithValue("@ShipperID", data.ShipperID);
                cmd.Parameters.AddWithValue("@Status", 1);


                orderId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return orderId;
        }

        public int AddOrderDetail(OrderDetail data)
        {
            int orderdetailsId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO dbo.OrderDetails
                                    (

                                            OrderID = @OrderID,
                                            ProductID = @ProductID,
                                            Quantity = @Quantity,
                                            SalePrice= @SalePrice
                                    )
                                    VALUES
                                    (  
                                        @OrderID,
                                      @ProductID,
                                      @Quantity,
                                        @SalePrice
                                        )
                                SELECT @@IDENTITY";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@OrderID", data.OrderID);
                cmd.Parameters.AddWithValue("@ProductID", data.ProductID);
                cmd.Parameters.AddWithValue("@Quantity", data.Quantity);
                cmd.Parameters.AddWithValue("@SalePrice", data.SalePrice);

                orderdetailsId = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }

            return orderdetailsId;
        }

        public int Count(DateTime? day, DateTime? today, int customerId, int statusId)
        {
            int count = 0;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT  COUNT(*)
                                        From Orders 
                                        WHERE ((OrderTime >= @day or OrderTime >= '1753-01-01') And (OrderTime <='9999-12-31' OR OrderTime <= @today))
                                        And (@customerId = 0 OR CustomerId = @customerId)
                                        AND (@statusId = 0 OR Status = @statusId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@customerId", customerId);
                cmd.Parameters.AddWithValue("@statusId", statusId);
                cmd.Parameters.AddWithValue("@day", day);
                cmd.Parameters.AddWithValue("@today", today);
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();

            }
            return count;
        }

        public bool Delete(int OrderId)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE From Orders where OrderID = @orderID";

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@orderID", OrderId);

                result = cmd.ExecuteNonQuery() > 0;

                connection.Close();
            }

            return result;
        }

        public bool DeleteOrderDetail(int orderdetailId)
        {
            throw new NotImplementedException();
        }




        public OrderEX GetEx(int OrderId)
        {
            throw new NotImplementedException();
        }

        public Order Get(int orderID)
        {
            Order data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM Orders WHERE OrderId = @OrderId";
                cmd.Parameters.AddWithValue("@OrderId", orderID);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {

                    if (dbReader.Read())
                    {
                        data = new Order()
                        {

                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            AcceptTime = string.IsNullOrEmpty(dbReader["AcceptTime"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dbReader["AcceptTime"]),
                            OrderTime = Convert.ToDateTime(dbReader["OrderTime"]),
                            ShipperID = (int)(string.IsNullOrEmpty(dbReader["ShipperID"].ToString()) ? (int?)null : Convert.ToInt32(dbReader["ShipperID"])),
                            ShippedTime = string.IsNullOrEmpty(dbReader["ShippedTime"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dbReader["ShippedTime"]),
                            Status = Convert.ToInt32(dbReader["Status"]),
                            FinishedTime = string.IsNullOrEmpty(dbReader["FinishedTime"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dbReader["FinishedTime"]),
                            EmployeeName = string.IsNullOrEmpty(dbReader["EmployeeName"].ToString()) ? (string)null : Convert.ToString(dbReader["EmployeeName"]),
                            CustomerName = string.IsNullOrEmpty(dbReader["CustomerName"].ToString()) ? (string)null : Convert.ToString(dbReader["CustomerName"]),
                            ShipperName = string.IsNullOrEmpty(dbReader["ShipperName"].ToString()) ? (string)null : Convert.ToString(dbReader["ShipperName"]),
                            OrderID = Convert.ToInt32(dbReader["OrderID"])


                        };
                    }
                }
                connection.Close();

            }
            return data;
        }

        public OrderDetailEX GetOderDetailEx(int OrderId)
        {
            throw new NotImplementedException();
        }

        public List<Order> List(int page, int pageSize, int statusId, int customerId, DateTime? day, DateTime? today)
        {

            List<Order> data = new List<Order>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT  *
                                    FROM
                                       (SELECT Orders.* ,ROW_NUMBER() OVER (ORDER BY FinishedTime) AS RowNumber
                                        FROM Orders INNER JOIN Customers ON Orders.CustomerID = dbo.Customers.CustomerID INNER JOIN OrderStatus ON  Orders.Status = OrderStatus.Status
                                        WHERE (@customerId = 0 OR Orders.CustomerID = @customerId)
                                            AND ( @statusId = 0 OR dbo.Orders.Status = @statusId ) 
                                            AND  ( (OrderTime >= @day OR @day = '1753-01-01') AND (@today ='1753-01-01' OR OrderTime <= @today)) )
                                            AS s 
                                       WHERE s.RowNumber BETWEEN (@page - 1)*@pageSize + 1 AND @page*@pageSize";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@statusId", statusId); 
                cmd.Parameters.AddWithValue("@customerId", customerId);
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@day", day);
                cmd.Parameters.AddWithValue("@today", today);
                cmd.Connection = connection;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {

                    while (dbReader.Read())
                    {
                        Order o = new Order()
                        {
                            OrderID = Convert.ToInt32(dbReader["OrderID"]),
                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            OrderTime = Convert.ToDateTime(dbReader["OrderTime"]),
                            Status = Convert.ToInt32(dbReader["Status"])
                        };
                        if (dbReader["AcceptTime"] == DBNull.Value)
                            o.AcceptTime = null;
                        else
                            o.AcceptTime = Convert.ToDateTime(dbReader["AcceptTime"]);
                        if (dbReader["FinishedTime"] == DBNull.Value)
                            o.FinishedTime = null;
                        else
                            o.FinishedTime = Convert.ToDateTime(dbReader["FinishedTime"]);
                        if (dbReader["ShippedTime"] == DBNull.Value)
                            o.ShippedTime = null;
                        else
                            o.ShippedTime = Convert.ToDateTime(dbReader["ShippedTime"]);
                        data.Add(o);
                    }
                }
                connection.Close();
            }
            return data;

        }
        public List<Order> ListCustomers()
        {
            List<Order> data = new List<Order>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Customers";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Order()
                        {
                            CustomerID = Convert.ToInt32(dbReader["CustomerID"]),
                            CustomerName = Convert.ToString(dbReader["CustomerName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<OrderDetail> ListOrderDetail(int orderdetailId)
        {
            throw new NotImplementedException();
        }


        public List<Order> ListProducts()
        {
            throw new NotImplementedException();
        }

        public List<Order> ListShippers()
        {
            List<Order> data = new List<Order>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Shippers";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Order()
                        {
                            ShipperID = Convert.ToInt32(dbReader["ShipperID"]),
                            ShipperName = Convert.ToString(dbReader["ShipperName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public int OrderStatus(OrderStatus data)
        {
            throw new NotImplementedException();
        }


        public bool UpdateOrderDetail(OrderDetail data)
        {
            throw new NotImplementedException();
        }



        public List<Order> ListEmployees()
        {
            List<Order> data = new List<Order>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Employees";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Order()
                        {
                            EmployeeID = Convert.ToInt32(dbReader["FistName"]),
                            ShipperName = Convert.ToString(dbReader["ShipperName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
    }
}
