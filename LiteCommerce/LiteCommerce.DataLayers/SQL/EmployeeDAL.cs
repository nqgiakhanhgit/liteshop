﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data;
using System.Data.SqlClient;

namespace LiteCommerce.DataLayers.SQL
{
    public class EmployeeDAL : _BaseDAL, IEmployee
    {
        public EmployeeDAL(string connectionString) : base(connectionString)
        {

        }
        /// <summary>
        /// Them
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public int Add(Employee data)
        {
            int EmployeeId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO dbo.Employees
				        ( LastName ,
				          FirstName ,
				          BirthDate ,
				          Photo ,
				          Notes ,
				          Email ,
				          Password
				        )
				VALUES  ( @LastName ,
                           @FirstName ,
				          GETDATE(),
				         @Photo ,
				          @Notes ,
                           @Email,
				          @Password
				        )
select @@IDENTITY"
;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@LastName", data.LastName);
                cmd.Parameters.AddWithValue("@FirstName", data.FirstName);
                cmd.Parameters.AddWithValue("@Photo", data.Photo);
                cmd.Parameters.AddWithValue("@Notes", data.Notes);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", data.Password);
                EmployeeId = Convert.ToInt32(cmd.ExecuteScalar());// 1 dong 1 cot 1 gia tri
                connection.Close();
            }
            return EmployeeId;
        }
        /// <summary>
        /// Dem So dong du lieu
        /// </summary>
        /// <param name="seachValue"></param>
        /// <returns></returns>
        public int Count(string seachValue)
        {
            if (seachValue != "")
            {
                seachValue = "%" + seachValue + "%";
            }
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select count (*)  from Employees where @employeeName like N'' or FirstName like @employeeName or LastName like @employeeName";
                cmd.Parameters.AddWithValue("@employeeName", seachValue);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                count = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// Xoa voi id
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public bool Delete(int supplierID)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE  FROM dbo.Employees where EmployeeId = @EmployeeId
                    AND NOT EXISTS(SELECT * FROM Orders
				WHERE EmployeeId = Employees.EmployeeID)";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@EmployeeId", supplierID);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// Lay Thong tin Employee voi id
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <returns></returns>
        public Employee Get(int EmployeeID)
        {
            Employee data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from Employees Where EmployeeID = @EmployeeID";
                cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Employee()
                        {
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            FirstName = Convert.ToString(dbReader["FirstName"]),
                            LastName = Convert.ToString(dbReader["LastName"]),
                            BirthDate = Convert.ToString(dbReader["BirthDate"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Notes = Convert.ToString(dbReader["Notes"]),
                            Password = Convert.ToString(dbReader["Password"]),
                            Photo = Convert.ToString(dbReader["Photo"])

                        };
                    }

                    connection.Close();
                }

            }
            ///reader
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="seachValue"></param>
        /// <returns></returns>

        public List<Employee> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Employee> data = new List<Employee>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from
                                    (
                                        select *, ROW_NUMBER() OVER(ORDER by FirstName) as RowNumber

                                        from Employees

                                        where (@searchValue = '')

                                            OR(
                                                FirstName LIKE @searchValue

                                                or LastName Like @searchValue
                                                
                                            )
                                    ) AS s
                                    where s.RowNumber between(@page -1)*@pageSize + 1 and @page*@pageSize
                                    Order by s.RowNumber";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Employee()
                        {
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            LastName = Convert.ToString(dbReader["LastName"]),
                            FirstName = Convert.ToString(dbReader["FirstName"]),
                            BirthDate = Convert.ToString(dbReader["BirthDate"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Password = Convert.ToString(dbReader["Password"]),
                            Notes = Convert.ToString(dbReader["Notes"]),
                            Photo = Convert.ToString(dbReader["Photo"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<Employee> List()
        {
            List<Employee> data = new List<Employee>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from Employees";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Employee()
                        {
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            LastName = Convert.ToString(dbReader["LastName"]),
                            FirstName = Convert.ToString(dbReader["FirstName"]),
                            BirthDate = Convert.ToString(dbReader["BirthDate"]),
                            Photo = Convert.ToString(dbReader["Photo"]),
                            Notes = Convert.ToString(dbReader["Notes"]),
                            Email = Convert.ToString(dbReader["Email"]),
                            Password = Convert.ToString(dbReader["Password"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        /// <summary>
        /// Cap Nhat thong tin
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Update(Employee data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.Employees 
    SET         FirstName = @FirstName,
                LastName = @LastName,
                BirthDate = @BirthDate,
                Email = @Email,
                Notes = @Notes,
                Password = @Password,
                Photo = @Photo
                Where EmployeeID = @EmployeeID ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@EmployeeID", data.EmployeeID);
                cmd.Parameters.AddWithValue("@FirstName", data.FirstName);
                cmd.Parameters.AddWithValue("@LastName", data.LastName);
                cmd.Parameters.AddWithValue("@BirthDate", data.BirthDate);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", data.Password);
                cmd.Parameters.AddWithValue("@Photo", data.Photo);
                cmd.Parameters.AddWithValue("@Notes", data.Notes);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
    }
}

