﻿using LiteCommerce.DataLayers.SQL;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class ProductDAL : _BaseDAL, IProductDAL
    {
        public ProductDAL(string connectionString) : base(connectionString)
        {
        }

        public int Add(Product data)
        {
            int productId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO Products
                                    (
	                                    ProductName,
	                                    SupplierID,
                                        CategoryID,
                                        Unit,
                                        Price,
                                        Photo
                                    )
                                    VALUES
                                    (
	                                    @ProductName,
	                                    @SupplierID,
                                        @CategoryID,
                                        @Unit,
                                        @Price,
                                        @Photo
                                    )
                                    SELECT @@IDENTITY";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@ProductName", data.ProductName);
                cmd.Parameters.AddWithValue("@SupplierID", data.SupplierID);
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@Price", data.Price);
                cmd.Parameters.AddWithValue("@Unit", data.Unit);
                cmd.Parameters.AddWithValue("@Photo", "http://dummyimage.com/250x250.png/ff4444/ffffff&text=Product Photo");


                productId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return productId;
        }

        public long AddGallery(ProductGallery data)
        {
            int galleryId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO ProductGallery (ProductID, Photo, Description, DisplayOrder, IsHidden)
                                            VALUES (@ProductID, @Photo, @Description, @DisplayOrder, @IsHidden)
                                            SELECT @@IDENTiTY";

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@ProductID", data.ProductID);
                cmd.Parameters.AddWithValue("@Photo", data.Photo);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", data.DisplayOrder);
                cmd.Parameters.AddWithValue("@IsHidden", data.IsHidden);

                galleryId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }

            return galleryId;
        }

        public long AddAttribute(ProductAttribute data)
        {
            
            int attributeId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO ProductAttributes (ProductID, AttributeName, AttributeValue, DisplayOrder)
                                            VALUES (@ProductID, @AttributeName, @AttributeValue, @DisplayOrder)
                                            SELECT @@IDENTiTY";

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@ProductID", data.ProductId);
                cmd.Parameters.AddWithValue("@AttributeName", data.AttributeName);
                cmd.Parameters.AddWithValue("@AttributeValue", data.AttributeValue);
                cmd.Parameters.AddWithValue("@DisplayOrder", data.DisplayOrder);

                attributeId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }

            return attributeId;
        }

        public int Count(int categoryID, int supplierID, string searchValue)
        {
            int count = 0;
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT  COUNT(*)

                                        FROM    Products 
                                        WHERE   (@categoryId = 0 OR CategoryId = @categoryId)
                                        AND  (@supplierId = 0 OR SupplierId = @supplierId)
                                        AND (@searchValue = '' OR ProductName LIKE @searchValue)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);
                cmd.Parameters.AddWithValue("@supplierId", supplierID);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }

        public bool Delete(int productId)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM Products
                                            WHERE(ProductID = @ProductID)
                                              AND(ProductID NOT IN(SELECT ProductID FROM OrderDetails))";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@ProductID", productId);

                int rowsAffected = cmd.ExecuteNonQuery();
                result = rowsAffected > 0;
                connection.Close();
            }
            return result;
        }

        public bool DeleteAttribute(long attributeID)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM ProductAttributes WHERE AttributeID = @AttributeID";

                cmd.Parameters.AddWithValue("@AttributeID", attributeID);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public bool DeleteGallery(long galleryID)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM ProductGallery WHERE GalleryID = @GalleryID";

                cmd.Parameters.AddWithValue("@GalleryID", galleryID);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public Product Get(int ProductID)
        {
            Product data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM Products WHERE ProductID = @ProductID";
                cmd.Parameters.AddWithValue("@ProductID", ProductID);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Product()
                        {
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            Unit = Convert.ToString(dbReader["Unit"]),
                            Price = Convert.ToInt32(dbReader["Price"]),
                            Photo = Convert.ToString(dbReader["Photo"]),
                        };
                    }
                }
                connection.Close();
            }
            return data;
        }

        public ProductAttribute GetAttribute(long attributeID)
        {
            ProductAttribute data = null;

            using (SqlConnection connection = GetConnection())  // kết nối sql
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT * FROM ProductAttributes WHERE AttributeID = @AttributeID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@AttributeID", attributeID);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new ProductAttribute()
                        {
                            AttributeID = Convert.ToInt32(dbReader["AttributeID"]),
                            ProductId = Convert.ToInt32(dbReader["ProductID"]),
                            AttributeName = Convert.ToString(dbReader["AttributeName"]),
                            AttributeValue = Convert.ToString(dbReader["AttributeValue"]),
                            DisplayOrder = Convert.ToInt32(dbReader["DisplayOrder"])
                        };
                    }
                }

                connection.Close();
            }

            return data;
        }

        public ProductEx GetEx(int ProductID)
        {
            Product product = Get(ProductID);
            if (product == null)
            {
                return null;
            }


            List<ProductAttribute> listAttributes = this.ListAttributes(ProductID);
            List<ProductGallery> listGallery = this.ListGalleries(ProductID);
            ProductEx data = new ProductEx()
            {
                ProductID = product.ProductID,
                CategoryID = product.CategoryID,
                Photo = product.Photo,
                Price = product.Price,
                ProductName = product.ProductName,
                SupplierID = product.SupplierID,
                Unit = product.Unit,

                Attributes = listAttributes,
                Galleries = listGallery
            };

            return data;
        }

        public ProductGallery GetGallery(long galleryID)
        {
            ProductGallery data = null;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT * FROM ProductGallery WHERE GalleryID = @GalleryID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@GalleryID", galleryID);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new ProductGallery()
                        {
                            GalleryID = Convert.ToInt32(dbReader["GalleryID"]),
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            Photo = Convert.ToString(dbReader["Photo"]),
                            Description = Convert.ToString(dbReader["Description"]),
                            DisplayOrder = Convert.ToInt32(dbReader["DisplayOrder"]),
                            IsHidden = Convert.ToBoolean(dbReader["IsHidden"])
                        };
                    }
                }

                connection.Close();
            }

            return data;
        }

        public List<Product> List(int page, int pageSize, int categoryID, int supplierID, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Product> data = new List<Product>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT  *
                                    FROM
                                    (
                                        SELECT  Products.*, ROW_NUMBER() OVER(ORDER BY ProductName) AS RowNumber, CategoryName, SupplierName
                                        FROM    Products inner join Categories on Products.CategoryID = Categories.CategoryID  inner join Suppliers on Products.SupplierID = Suppliers.SupplierID
                                        WHERE   (@categoryId = 0 OR Products.CategoryID = @categoryId)
                                        AND  (@supplierId = 0 OR Products.SupplierID = @supplierId)
                                        AND (@searchValue = '' OR ProductName LIKE @searchValue)
                                    ) AS s
                                    WHERE s.RowNumber BETWEEN (@page - 1)*@pageSize + 1 AND @page*@pageSize";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);
                cmd.Parameters.AddWithValue("@supplierId", supplierID);
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            SupplierName = Convert.ToString(dbReader["SupplierName"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            Unit = Convert.ToString(dbReader["Unit"]),
                            Price = Convert.ToDecimal(dbReader["Price"]),
                            Photo = Convert.ToString(dbReader["Photo"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<ProductAttribute> ListAttributes(int productID)
        {
            List<ProductAttribute> data = new List<ProductAttribute>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM ProductAttributes 
                                   WHERE ProductID = @productId 
                                   ORDER BY DisplayOrder asc";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@productId", productID);
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new ProductAttribute()
                        {
                            AttributeID = Convert.ToInt32(dbReader["AttributeID"]),
                            ProductId = Convert.ToInt32(dbReader["ProductID"]),
                            AttributeName = Convert.ToString(dbReader["AttributeName"]),
                            AttributeValue = Convert.ToString(dbReader["AttributeValue"]),
                            DisplayOrder = Convert.ToInt32(dbReader["DisplayOrder"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }

        public List<ProductGallery> ListGalleries(int productID)
        {
            List<ProductGallery> data = new List<ProductGallery>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM ProductGallery WHERE ProductID = @productId order by DisplayOrder ASC";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@productId", productID);
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new ProductGallery()
                        {
                            GalleryID = Convert.ToInt32(dbReader["GalleryID"]),
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            Photo = Convert.ToString(dbReader["Photo"]),
                            Description = Convert.ToString(dbReader["Description"]),
                            DisplayOrder = Convert.ToInt32(dbReader["DisplayOrder"]),
                            IsHidden = Convert.ToBoolean(dbReader["IsHidden"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }

        public bool Update(Product data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {

                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE Products
                                            SET ProductName = @ProductName,
                                            Unit = @Unit,
                                            Price = @Price
                                    WHERE ProductID = @ProductID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@ProductID", data.ProductID);
                cmd.Parameters.AddWithValue("@ProductName", data.ProductName);
                cmd.Parameters.AddWithValue("@Unit", data.Unit);
                cmd.Parameters.AddWithValue("@Price", data.Price);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }

        public bool UpdateAttribute(ProductAttribute data)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE ProductAttributes 
                                        SET
                                        ProductID = @ProductID,
                                        AttributeName = @AttributeName,
                                        AttributeValue = @AttributeValue,
                                        DisplayOrder = @DisplayOrder
                                        where AttributeID = @AttributeID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@AttributeID", data.AttributeID);
                cmd.Parameters.AddWithValue("@ProductID", data.ProductId);
                cmd.Parameters.AddWithValue("@AttributeName", data.AttributeName);
                cmd.Parameters.AddWithValue("@AttributeValue", data.AttributeValue);
                cmd.Parameters.AddWithValue("@DisplayOrder", data.DisplayOrder);
               

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public bool UpdateGallery(ProductGallery data)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE ProductGallery 
                                        SET 
                                        ProductID = @ProductID,
                                        Photo = @Photo,
                                        Description = @Description,
                                        DisplayOrder = @DisplayOrder,
                                        IsHidden = @IsHidden
                                        where GalleryID = @GalleryID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ProductID", data.ProductID);
                cmd.Parameters.AddWithValue("@GalleryID", data.GalleryID);
                cmd.Parameters.AddWithValue("@Photo", data.Photo);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@IsHidden", data.IsHidden);
                cmd.Parameters.AddWithValue("@DisplayOrder", data.DisplayOrder);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public List<Product> ListCategories()
        {
            List<Product> data = new List<Product>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Categories";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<Product> ListSuppliers()
        {
            List<Product> data = new List<Product>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Suppliers";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            SupplierName = Convert.ToString(dbReader["SupplierName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
    }
}
