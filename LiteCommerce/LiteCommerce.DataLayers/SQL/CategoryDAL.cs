﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data;
using System.Data.SqlClient;

namespace LiteCommerce.DataLayers.SQL
{
    public class CategoryDAL: _BaseDAL,ICategory
    {
        public CategoryDAL(string connectionString) : base(connectionString)
        {

        }
        /// <summary>
        /// Them
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public int Add(Category data)
        {
            int CategoryId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO dbo.Categories
        ( CategoryName ,
          Description,
          ParentCategoryId
        )

VALUES  (
		@CategoryName,
         @Description,
          @ParentCategoryId
        )
select @@IDENTITY"
;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);
                CategoryId = Convert.ToInt32(cmd.ExecuteScalar());// 1 dong 1 cot 1 gia tri
                connection.Close();
            }
            return CategoryId;
        }
        /// <summary>
        /// Dem So dong du lieu
        /// </summary>
        /// <param name="seachValue"></param>
        /// <returns></returns>
        public int Count(string seachValue)
        {
            if (seachValue != "")
            {
                seachValue = "%" + seachValue + "%";
            }
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select count (*)  from Categories where @seachValue like N'' or CategoryName like @seachValue or Description like @seachValue";
                cmd.Parameters.AddWithValue("@seachValue", seachValue);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                count = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// Xoa voi id
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public bool Delete(int categoryID)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE  FROM dbo.Categories where categoryId = @categoryId
                    AND NOT EXISTS(SELECT * FROM Products
				WHERE CategoryID = Categories.CategoryID)";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// Lay Thong tin supplier voi id
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public Category Get(int categoryId)
        {
            Category data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from Categories Where CategoryID = @categoryId";
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Category()
                        {
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            Description = Convert.ToString(dbReader["Description"]),
                         //   ParentCategoryId = Convert.ToInt32(dbReader["ParentCategoryId"]),
                            };
                    }

                    connection.Close();
                }

            }
            ///reader
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="seachValue"></param>
        /// <returns></returns>
        
        public List<Category> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Category> data = new List<Category>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from
                                    (
                                        select *, ROW_NUMBER() OVER(ORDER by CategoryName) as RowNumber

                                        from Categories

                                        where (@searchValue = '')

                                            OR(
                                                CategoryName LIKE @searchValue

                                                or Description Like @searchValue
                                                
                                            )
                                    ) AS s
                                    where s.RowNumber between(@page -1)*@pageSize + 1 and @page*@pageSize
                                    Order by s.RowNumber";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Category()
                        {
                            CategoryID = Convert.ToInt32(dbReader["CategoryId"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            Description = Convert.ToString(dbReader["Description"]),
                            
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }        /// <summary>
                 /// Cap Nhat thong tin
                 /// </summary>
                 /// <param name="data"></param>
                 /// <returns></returns>
        public bool Update(Category data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.Categories 
SET CategoryName =@CategoryName,
	Description=@Description,
	ParentCategoryId = @ParentCategoryId,
	
	WHERE CategoryID = @CategoryID";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);
                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
    }
}
