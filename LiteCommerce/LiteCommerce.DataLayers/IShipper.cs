﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data.Sql;
using System.Data.SqlClient;
namespace LiteCommerce.DataLayers
{
    public interface IShipper
    {

        List<Shipper> List(int page, int pageSize, string seachValue);
        /// <summary>
        /// Đếm số lượng nhà cung cấp
        /// </summary>
        /// <param name="seachValue">Giá trị cần tìm kiếm (để rỗng lấy toàn bộ)</param>
        /// <returns></returns>
        /// 

        int Count(string seachValue);

        /// <summary>
        /// Lấy Thông tin của một nhà cung cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 

       Shipper Get(int shipperId);

        /// <summary>
        /// Bổ sung 1 nhà cung cấp trả về mã của nhà cung cấp cần được bổ sung
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>

        int Add(Shipper data);

        /// <summary>
        /// Cập nhật thông tin nhà cung cấp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Update(Shipper data);

        /// <summary>
        /// Xóa Một nhà cung cấp
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        bool Delete(int shipperID);
    }
}

