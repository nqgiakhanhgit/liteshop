﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// Khai Báo Chức năng xử lý dữ liệu liên quan đến Thành Phố
    /// </summary>
    public interface ICityDAL
    {
        /// <summary>
        /// Lấy Danh Sách thành phố
        /// </summary>
        /// <returns></returns>
        List<City>List(Country name);
        /// <summary>
        /// Lấy danh sách thành phố thuộc quốc gia
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        List<City> List();
    }
}
