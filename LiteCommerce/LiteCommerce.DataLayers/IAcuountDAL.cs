﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// Định nghĩa các phép xử lý liên quan đến tài khoảng đăng nhập vào hệ thống
    /// </summary>
   public interface IAcuountDAL
    {
        /// <summary>
        /// kiểm tra thông tin đăng nhập. Nếu đăng nhập đúng thì trả về đối tượng kiểu accout (Thông tin của tài khoảng đăng nhập)
        /// ngược lại trả về null
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Account Authorize(string userName, string password);
        /// <summary>
        /// Thày đổi mật khẩu
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        bool ChangePassword(string accountId, string oldPassword, string newPassword);
    }
}
