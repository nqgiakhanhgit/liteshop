﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int SupplierID { get; set; }
        public int CategoryID { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
        public string SupplierName { get; set; }
        public string CategoryName { get; set; }




    }
    /// <summary>
    /// Mặt hàng nhưng có một số thông tin bổ sung 
    /// </summary>
    public class ProductEx : Product
    {
        public List<ProductAttribute> Attributes { get; set; }
        public List<ProductGallery>Galleries { get; set; }
    }
}
