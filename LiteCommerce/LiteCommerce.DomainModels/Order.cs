﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;

namespace LiteCommerce.DomainModels
{
    public class Order
    {
        public int OrderID { get; set; }
        public int CustomerID { get; set; }


       
        public DateTime? OrderTime { get; set; }

        public int EmployeeID { get; set; }
        
        public Nullable<DateTime> AcceptTime { get; set; }
        public Nullable<int> ShipperID { get; set; }

        public Nullable<DateTime> ShippedTime { get; set; }

        public Nullable<DateTime> FinishedTime { get; set; }
        public int? Status { get; set; }
        public string CustomerName { get; set; }

        public string EmployeeName { get; set; }

        public string ShipperName { get; set; }
    }
}
public class OrderEX
{
    public List<Customer> Customers { get; set; }
    public List<Shipper> Shippers { get; set; }

    public List<Employee> Employees { get; set; }
}
