﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    /// <summary>
    /// lưu giữ thông tin tài khoảng đang đăng nhập vào hệ thống
    /// Employee,Customer
    /// </summary>
   public class Account
    {
        /// <summary>
        /// mã tài khoảng của employee, mã của customer
        /// nếu là employee thì employeeid, customer thì customerid
        /// </summary>
        public string AccountID { get; set; }
        /// <summary>
        /// Tên gọi 
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// photo
        /// </summary>
        public string photo { get; set; }
    }
}
