﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    public class Supplier
    {
        /// <summary>
        /// ID
        /// </summary>
        public int SupplierID { get; set; }
        /// <summary>
        /// Tên 
        /// </summary>
        public string SupplierName { get; set; }
        /// <summary>
        /// Tên Giao Dịch
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// Địa Chỉ
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Thành Phố
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Mã Vùng, Mã Bưu Chính
        /// </summary>
        public string PostalCode { get; set; }
        /// <summary>
        /// Nước
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Điện Thoại
        /// </summary>
        public string Phone { get; set; }
    }
}
