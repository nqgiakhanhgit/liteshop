﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
   public class City
    {
        /// <summary>
        /// Tên thành Phố
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        ///  và thành phố thuộc quốc gia nào ? 
        /// </summary>
        public string CountryName { get; set; }
    }
}
